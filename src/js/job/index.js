/**
 * 首页
 */
define(function(){
    var pagejob = require("../modules/pagejob/index");
    var jobs = require("../units/common/index");
    // 各有任务组件js
    jobs.push(require("../units/main/index"));
    
    for (var i = 0; i < jobs.length; i++) {
        var job = jobs[i];
        pagejob.add(job);
    }
    pagejob.start();
});
