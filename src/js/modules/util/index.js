/**
 * 
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.util = factory();
    }
}(this, function () {
    var localStorage = window.localStorage;
    var sessionStorage = window.sessionStorage;
    return {
        /*任意进制加密*/
        EnChTo: function (h, data) {
            var monyer = [];
            var i, s;
            for (i = 0; i < data.length; i++) {
                monyer += data.charCodeAt(i).toString(h) + " ";
            }
            return monyer.toString();
        },
        /*任意进制解密*/
        DeChTo: function (h, data) {
            var monyer = [];
            var i;
            var s = data.split(" ");
            for (i = 0; i < s.length; i++) {
                monyer += String.fromCharCode(parseInt(s[i], h));
            }
            return monyer.toString();
        },
        setCookie: function (key, data, type, opt) {
            if (data == "" || data == null) {
                return;
            }
            if (type == "json" && typeof(data) == "object") {
                $.cookie(key, this.EnChTo(16, encodeURI(JSON.stringify(data))), opt || {path: '/'});
            } else if (type != "json") {
                $.cookie(key, this.EnChTo(16, encodeURI(data)), opt || {path: '/'});
            }
        },
        getCookie: function (key, type) {
            var data = $.cookie(key);
            if (data == null || data == 'null') {
                return null;
            }
            data = this.DeChTo(16, data);
            data = decodeURI(data);
            if (type == "json") {
                return JSON.parse(data.substring(0, data.length - 2) + "}");
            } else {
                return data.substring(0, data.length - 1);
            }
        },
        removeCookie: function (key, opt) {
            return $.cookie(key, null, opt || {path: '/'});
        },
        setLocalStorage: function (key, data, type) {
            if (data == "" || data == null) {
                return;
            }
            if (localStorage) {
                if (type == "json" && typeof(data) == "object") {
                    localStorage.setItem(key, this.EnChTo(16, encodeURI(JSON.stringify(data))));
                } else if (type != "json") {
                    localStorage.setItem(key, this.EnChTo(16, encodeURI(data)));
                }
            } else {
                this.setCookie(key, data, type, {path: '/',expires:3650});
            }
        },
        getLocalStorage: function (key, type) {
            var data = localStorage ? localStorage.getItem(key) : $.cookie(key);
            if (data == null || data == 'null') {
                return null;
            }
            data = this.DeChTo(16, data);
            data = decodeURI(data);
            if (type == "json") {
                return JSON.parse(data.substring(0, data.length - 2) + "}");
            } else {
                return data.substring(0, data.length - 1);
            }
        },
        removeLocalStorage: function (key) {
            return localStorage ? localStorage.removeItem(key) : $.cookie(key, null, {path: '/',expires:3650});
        },
        setSessionStorage: function (key, data, type) {
            if (data == "" || data == null) {
                return;
            }
            if (sessionStorage) {
                if (type == "json" && typeof(data) == "object") {
                    sessionStorage.setItem(key, this.EnChTo(16, encodeURI(JSON.stringify(data))));
                } else if (type != "json") {
                    sessionStorage.setItem(key, this.EnChTo(16, encodeURI(data)));
                }
            } else {
                this.setCookie(key, data, type);
            }
        },
        getSessionStorage: function (key, type) {
            var data = sessionStorage ? sessionStorage.getItem(key) : $.cookie(key);
            if (data == null || data == 'null') {
                return null;
            }
            data = this.DeChTo(16, data);
            data = decodeURI(data);
            if (type == "json") {
                return JSON.parse(data.substring(0, data.length - 2) + "}");
            } else {
                return data.substring(0, data.length - 1);
            }
        },
        removeSessionStorage: function (key) {
            return sessionStorage ? sessionStorage.removeItem(key) : $.cookie(key, null);
        },
        getUrlParam : function(name){
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return r[2]; return null;
        },
        getHashParam : function(name){
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.hash.substr(1).match(reg);
            if (r != null) return r[2]; return null;
        },
        // 获取js文件
        loadScript: function (src) {
            var fm = document.createElement('script');
            fm.type = 'text/javascript';
            fm.async = true;
            fm.src = src;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fm, s);
        }
    }
}));

