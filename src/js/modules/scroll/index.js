(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.Scroll = factory();
    }
}(this, function () {
    /*
        大轮播
     */
　　function Scroll(options){
        this.scrollWrap = options.$scrollWrap;//滚动区外层
        this.scrollElement = this.scrollWrap.children();//滚动元素 ul
        this.scrollLength = this.scrollElement.children().length;//滚动个数 li
        this.scrollIndex = options.scrollIndex || 0;//初始化滚动索引

        this.template();//构建页码组件

        var visibleWidth = document.documentElement.clientWidth || document.body.clientWidth;//可视区宽度
        var self = this;
        (visibleWidth <= options.minWidth) && (visibleWidth = options.minWidth);
        //修正scrollWidth宽度
        this.scrollElement.width(function(){
            return visibleWidth * self.scrollLength
        });
        // ul li a img
        this.scrollWidth  = this.scrollElement
                                .children().children().children()
                                .width(visibleWidth).show().width();//滚动宽度，修正闪烁
        // 显示
        this.scrollWrap.css({
            visibility:'visible'
        });

        this.scrollPoint = this.scrollWrap.children('.scroll-btn-list').children();//小按钮
        this.scrollBtn = this.scrollWrap.children('.scroll-btn')//切换按钮
        this.autoInt = null;

        //小按钮事件
        this.pointEvent();
        //切换按钮事件
        this.btnEvent();
        //自动轮播
        this.autoScroll();
    }
    Scroll.prototype = {
        constructor:Scroll,
        scrollEvent:function(){
            //图片区
            var that = this;
            this.scrollElement.stop().animate({
                marginLeft: -that.scrollIndex*that.scrollWidth,
                opacity: .5
            },0,function(){
                $(this).animate({
                    opacity: 1
                }, 1000);
            });
            //小按钮区
            this.scrollPoint.eq(this.scrollIndex).removeClass('icon-img-point').addClass('icon-img-point-current').siblings().removeClass('icon-img-point-current').addClass('icon-img-point');
        },
        pointEvent:function(){
            var that = this;
            this.scrollPoint.on({
                mouseover:function(){
                    that.scrollIndex = $(this).index();
                    that.scrollEvent();
                    clearInterval(that.autoInt);
                },
                mouseout:function(){
                    that.autoScroll();
                }
            })
        },
        btnEvent:function(){
            var that = this;
            this.scrollBtn.on({
                click:function(event){
                    event.stopPropagation();
                    if($(this).hasClass('icon-img-pre')){
                        if(--that.scrollIndex === -1){
                            that.scrollIndex = that.scrollLength-1;//循环
                        }
                    }else if($(this).hasClass('icon-img-next')){
                        if(++that.scrollIndex === that.scrollLength){
                            that.scrollIndex = 0;//循环
                        }
                    }
                    that.scrollEvent();
                },
                mouseover:function(){
                    clearInterval(that.autoInt);
                },
                mouseout:function(){
                    that.autoScroll();
                }
            });

            this.scrollWrap.on({
                mouseenter:function(){
                    that.scrollBtn.animate({
                        opacity: 1
                    },300).show();
                },mouseleave:function(){
                    that.scrollBtn.animate({
                        opacity: 0
                    },300).hide();
                }
            });
        },
        autoScroll:function(){
            var that = this;
            this.autoInt = setInterval(function(){
                if(++that.scrollIndex === that.scrollLength){
                    that.scrollIndex = 0;
                }
                that.scrollEvent();
            },6000);
        },
        template:function(){
            var html = '<ul class="scroll-btn-list">';
            for(var i = 0; i < this.scrollLength; i++){
                html += '<li class="icon-img-point"></li>';
            }
            html += '</ul>'+
                    '<span class="scroll-btn icon-img-pre"></span>'+
                    '<span class="scroll-btn icon-img-next"></span>';
            this.scrollElement.after(html).siblings('.scroll-btn-list')
                .children().eq(this.scrollIndex).removeClass('icon-img-point').addClass('icon-img-point-current');
        }
    }

    /*
        小轮播
     */
    function MiniScroll(options){
        this.scrollWrap = options.$scrollWrap;//滚动区外层
        this.scrollElement = this.scrollWrap.children().eq(0);//滚动元素
        this.scrollLength = this.scrollElement.children().length;//滚动个数
        this.scrollIndex = options.scrollIndex || 0;//初始化滚动索引

        this.template();//构建组件

        this.scrollWidth  = this.scrollElement.children().width();//滚动宽度
        this.scrollElement.width(this.scrollWidth * this.scrollLength);//修正scrollWidth宽度


        this.scrollPointWrap = this.scrollWrap.children().eq(1);//小按钮外层
        this.scrollPoint = this.scrollPointWrap.children();//小按钮

        this.autoInt = null;

        //小按钮事件
        this.pointEvent();
        //自动轮播
        this.autoScroll();
    }
    MiniScroll.prototype = {
        constructor:MiniScroll,
        scrollEvent:function(){
            var that = this;
            
            //小按钮区
            this.scrollPoint.eq(this.scrollIndex).removeClass('icon-img-point-small').addClass('icon-img-point-current-small').siblings().removeClass('icon-img-point-current-small').addClass('icon-img-point-small');
            //图片区
            this.scrollElement.stop().animate({
                marginLeft: -that.scrollIndex*that.scrollWidth,
                opacity: .5
            },0,function(){
                $(this).animate({
                    opacity: 1
                }, 600);
            });
        },
        pointEvent:function(){
            var that = this;
            this.scrollPoint.on({
                mouseover:function(){
                    that.scrollIndex = $(this).index();
                    that.scrollEvent();
                    clearInterval(that.autoInt);
                },
                mouseout:function(){
                    that.autoScroll();
                }
            })
        },
        autoScroll:function(){
            var that = this;
            this.autoInt = setInterval(function(){
                if(++that.scrollIndex === that.scrollLength){
                    that.scrollIndex = 0;
                }
                that.scrollEvent();
            },3000);
        },
        template:function(){
            var html = '<div class="point-btn">';
            for(var i = 0; i < this.scrollLength; i++){
                html += '<span class="icon-img-point-small"></span>';
            }
            html += '</div>';

            this.scrollElement.after(html).siblings('.point-btn')
                .children().eq(this.scrollIndex).removeClass('icon-img-point-small').addClass('icon-img-point-current-small');
        }
    }

    /*
        SlideUp 注意耦合性
     */
    function SlideUp(options){
        this.$slideWrap = options.$slideWrap;//向上滑动区外层
        this.$switch = options.$switch;//按钮
        this.slideIndex = options.slideIndex || 0;//默认显示第一屏
        
        this.$slideElement = this.$slideWrap.children().eq(0);//滚动元素 ul
        this.slideLength = this.$slideElement.children().length;//滚动个数 li
        this.distance = this.$slideElement.children().eq(0).outerHeight() * 5;//滑动的距离
        this.slideNum = Math.ceil(this.slideLength / 5);//总屏数

        this.$totalPage = options.$totalPage;
        this.$currentPage = options.$currentPage;

        this.init();
        this.bindEvent();
    }
    SlideUp.prototype = {
        constructor:SlideUp,
        init:function(){
            var self = this;
            this.$slideElement.stop(true, true).animate({
                top : -self.slideIndex * self.distance
            }, 0, function(){});

            this.$totalPage.html(this.slideNum);
            this.$currentPage.html(this.slideIndex + 1);
        },
        slide:function(){
            var self = this;
            self.$slideElement.stop(true, true).animate({
                top : -self.slideIndex * self.distance
            }, 600, function(){
                self.$currentPage.html(self.slideIndex + 1);
            });
        },
        bindEvent:function(){
            var self = this;
            this.$switch.on('click', '.icon-arrow-pre', function(event){
                if(self.slideIndex<=0){
                    // $(this).css({
                    //     visibility: 'hidden'
                    // });
                    return;
                }
                --self.slideIndex;
                self.slide();
            });
            this.$switch.on('click', '.icon-arrow-next', function(event){
                if(self.slideIndex>=self.slideNum-1){
                    // $(this).css({
                    //     visibility: 'hidden'
                    // });
                    return;
                }
                ++self.slideIndex;
                self.slide();
            });
        }

    }
    return {
        Scroll : Scroll,
        MiniScroll : MiniScroll,
        SlideUp : SlideUp
    }
}));