(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.top = factory();
    }
}(this, function () {
    //当前验证码获取的随即范围
    var codeStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    //思想：0-61索引 只需要随机生成4个索引，然后charAt可以获取随机4个索引。
    var oDiv = document.getElementById("code");

    function getRandom(n, m) {
        n = Number(n);       //转换n,m，结果不是数字就是NaN
        m = Number(m);
        if (isNaN(n) || isNaN(m)) {     //判断n,m,是不是有效数字，如果n或m其中一个传入的不是数字
            return Math.random();      //返回 【0-1）之间的随机小数
        }
        if (n > m) {             //如果n大于m，则交换位置
            var temp = n;
            n = m;
            m = temp;
        }
        return Math.round(Math.random() * (m - n) + n);          //返回，取m,n之间的随机整数。
    };


    function getCode() {
        var str = "";　　　　　　　　//定义一个空字符串备用
        for (var i = 0; i < 4; i++) {    //遍历4个索引
            var ran = getRandom(0, 61);      //调用getRandom方法，随机获取一个索引0-61里的随机索引
            str += codeStr.charAt(ran);      //把codeStr字符串里，我们指定获取ran（这个4个索引）；
        }
        oDiv.innerHTML = str;       //呈现在页面上
        oDiv.className="picvalite";
    };

    if(oDiv==null){
        return false;
    }else{
        getCode(); //调用方法
        oDiv.onclick = function () {
            getCode();
        };
    };

}));