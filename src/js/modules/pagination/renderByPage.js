
/*
    分页组件
 */

define(function(){
    var config = require('../../config/index');
    // 组件
    var Page = require('./index'),
        Runtime = require('../../modules/runtime/index');

    return function(tplPath, url, firstPage, pageSize){
        var page = null;

        /*
            第一次加载获取用于page的参数
         */
        function firstExcec(){
            var dtd = $.Deferred();
            Runtime({
                tplPath:tplPath,
                beforeStart:function(){},
                param:{
                    url:url,
                    data:{
                        curPage:firstPage,
                        pageSize:pageSize
                    }
                },
            }).done(function(html, data){
                if(data.statusCode!=200){
                    dtd.reject(html, data);//处理失败
                }else{
                    dtd.resolve(html, data);
                }
            }).fail(function(){
                dtd.reject();
            });
            return dtd.promise();
        }

        // 第一次拉取成功之后，实例化page
        return function($videoListWrap, $paginationWrap, successFn){
            firstExcec().then(function(html, data){
                page = new Page({
                    language:config.lang,
                    maxPage:data.page.pageCount,//最大页码数
                    initPage:data.page.pageIndex,//当前页码数
                    $paginationWrap:$paginationWrap,//插入页码的位置
                    pageSize:pageSize,
                    afterfn:function(){
                        $videoListWrap.html(html);
                        successFn.call(null);
                    },
                    callBack:function(clickPageValue){
                        Runtime({
                            tplPath:tplPath,
                            beforeStart:function(){},
                            param:{
                                url:url,
                                data:{
                                    curPage:clickPageValue,
                                    pageSize:this.pageSize
                                }
                            },
                        }).done(function(html, data){
                            if(data.statusCode!=200){
                                alert(data.message||'请求错误');
                                return;
                            }
                            $videoListWrap.html(html);
                            // 最大的页码数 发生变化时 更新页码
                            if(page.maxPage != data.page.pageCount){
                                page.maxPage = data.page.pageCount;
                                page.totalPage();
                                page.$paginationList.html(function(){
                                    return page.pageGender(clickPageValue);
                                });
                            }
                        });
                    }
                });
            }, function(){
                console.log('这个是第一次加载就出错');
            });
        }
    }
});