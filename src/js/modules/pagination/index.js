
define(function(){
    function Page(options){
        this.language = options.language || 'zh';
        this.maxPage = options.maxPage || 0;
        this.pageSize = options.pageSize || 20; //提供 10 20 50分页页码
        this.$paginationWrap = options.$paginationWrap;//插入页码组件的位置
        this.initPage = options.initPage || 1;
        this.beforefn = options.beforefn || function(){};//
        this.afterfn = options.afterfn || function(){};//
        this.initefn  = options.initefn || function(){};//初始化函数
        this.execFnNotByFirst = options.callBack || function(){};//非第一次才调用的函数
        this.template();
        this.$totalPage = this.$paginationWrap.find('.total-pages .pages-num');//总页码
        this.$pageSelect = this.$paginationWrap.find('.page-select').find('select');//pageSize选择框
        this.$paginationList = this.$paginationWrap.find('.pagination-list');// 页码列表

        this.isFirst = true;
        if(this.maxPage){
            this.inite();
        }
    }
    Page.prototype.inite = function(){
        this.beforefn();

        this.totalPage();
        this.selectPageSize();
        this.pageInite();
        this.pageEvent();
        this.pageGo();
        this.preOrNext();
        this.$paginationWrap.find('.pagination').show();
        
        this.isFirst = false;
        
        return this;
    }
    // 构建DOM
    Page.prototype.template = function(){
        if(this.language=='zh'){
            var html = '<ul class="pagination" onselectstart="return false;">'+
                            '<li class="total-pages">'+
                                '共'+
                                '<span class="pages-num">0</span>'+
                                '页'+
                            '</li>'+
                            '<li class="page-select">'+
                                '<select class="form-control">'+
                                    '<option value="10">10</option>'+
                                    '<option value="20">20</option>'+
                                    '<option value="50">50</option>'+
                                '</select>'+
                            '</li>'+
                            '<li class="page-btn btn-pre">上一页</li>'+
                            '<li class="pagination-list">'+
                                // <a href="#" class="page-item page-current">1</a>
                                // <a href="#" class="page-item">2</a>
                                // <a href="#" class="page-item">3</a>
                                // <a href="#" class="page-item">4</a>
                                // <a href="#" class="page-item">5</a>
                                // <a href="#" class="page-item">6</a>
                                // <a href="#" class="page-item">7</a>
                            '</li>'+
                            '<li class="page-btn btn-next">下一页</li>'+
                            '<li class="pages-value">'+
                                '跳到第'+
                                '<input class="form-control" type="text" name="" value="">'+
                                '页'+
                            '</li>'+
                            '<li class="pages-go">GO</li>'+
                        '</ul>';
        }else if(this.language=='en'){
            var html = '<ul class="pagination" onselectstart="return false;">'+
                            '<li class="total-pages">'+
                                'Total pages:'+
                                '<span class="pages-num">0</span>'+
                                // '页'+
                            '</li>'+
                            '<li class="page-select">'+
                                '<select class="form-control">'+
                                    '<option value="10">10</option>'+
                                    '<option value="20">20</option>'+
                                    '<option value="50">50</option>'+
                                '</select>'+
                            '</li>'+
                            '<li class="page-btn btn-pre">previous</li>'+
                            '<li class="pagination-list">'+
                                // <a href="#" class="page-item page-current">1</a>
                                // <a href="#" class="page-item">2</a>
                                // <a href="#" class="page-item">3</a>
                                // <a href="#" class="page-item">4</a>
                                // <a href="#" class="page-item">5</a>
                                // <a href="#" class="page-item">6</a>
                                // <a href="#" class="page-item">7</a>
                            '</li>'+
                            '<li class="page-btn btn-next">next</li>'+
                            '<li class="pages-value">'+
                                'GoTo'+
                                '<input class="form-control" type="text" name="" value="">'+
                                // '页'+
                            '</li>'+
                            '<li class="pages-go">GO</li>'+
                        '</ul>';
        }
        this.$paginationWrap.html(function(){
            return html;
        });
    }
    // 设置pagesize
    Page.prototype.selectPageSize = function(){
        var self = this;
        this.$paginationWrap.find('.page-select').find('select').val( this.pageSize )[0].selected=true;
        this.$pageSelect.on('change', function(event){
            self.pageSize = $(this).val();
            self.$paginationList.html( self.pageClick(1) );
        });
    }
    //总页数
    Page.prototype.totalPage = function(){
        this.$totalPage.html( this.maxPage );
    }
    //页码初始状态 相当于点击了第一页
    Page.prototype.pageInite = function(){
        this.$paginationList.html( this.pageClick(this.initPage) );
    }
    //页码点击事件
    Page.prototype.pageEvent = function(){
        var self = this;
        this.$paginationWrap.on('click', '.pagination-list a', function(event){
             event.preventDefault();
             // 过滤前后 和 中间的空格
             var pageNum = Number($(this).html().replace(/(^\s+)|(\s+$)/g, '').replace(/\s/g, ''));
             if(!isNaN(pageNum)){//过滤省略号
                 self.$paginationList.html('').html( self.pageClick(pageNum) );
             }
        });
    }
    // 页码pagelist 动态js画
    Page.prototype.pageGender = function(pageNum){
        var maxPage = this.maxPage,
            pageList = '';

        if(pageNum>maxPage){
            pageNum = maxPage;
        }else if(pageNum<=0){
            pageNum = 1;
        }

        if(maxPage>7){
            if(pageNum<5){//1~4点击不变
                for (var i = 1; i <= 5; i++) {
                    if(i===pageNum){
                        pageList += '<a class="page-item page-current" href="#">'+i+'</a>';
                        continue;
                    }
                    pageList += '<a class="page-item" href="#">'+i+'</a>';

                };
                pageList += '<a class="page-more" href="#">'+'···'+'</a>'+
                            '<a class="page-item" href="#">'+maxPage+'</a>';
            }else if(pageNum>(maxPage-4)){//后4个点击不变
                pageList += '<a class="page-item" href="#">'+1+'</a>'+
                            '<a class="page-more" href="#">'+'···'+'</a>';
                for (var i = 4; i > -1; i--) {
                    if(pageNum===(maxPage-i)){
                        pageList += '<a class="page-item page-current" href="#">'+pageNum+'</a>';
                        continue;
                    }
                    pageList += '<a class="page-item" href="#">'+(maxPage-i)+'</a>';
                };
            }else{//5~N
                if((pageNum-3)>2){
                    pageList += '<a class="page-item" href="#">'+1+'</a>'+
                                '<a class="page-more" href="#">'+'···'+'</a>';
                    for(var i=2;i>=1;i--){
                        pageList += '<a class="page-item" href="#">'+(pageNum-i)+'</a>';
                    }
                }else{
                    for(var i =1;i<pageNum;i++){
                        pageList += '<a class="page-item" href="#">'+i+'</a>';
                    }
                }

                pageList += '<a class="page-item page-current" href="#">'+pageNum+'</a>';
                
                if((pageNum+4)>=maxPage){
                    var tmp = maxPage-pageNum;
                    for(var j=1;j<=tmp;j++){
                        pageList += '<a class="page-item" href="#">'+(pageNum+j)+'</a>';
                    }
                }else{
                    for(var j =1;j<=2;j++){
                        pageList += '<a class="page-item" href="#">'+(pageNum+j)+'</a>';
                    }
                    pageList += '<a class="page-more" href="#">'+'···'+'</a>'+
                                '<a class="page-item" href="#">'+maxPage+'</a>';
                }
            }
        }else{//小于7页全部显示
            for (var i = 1; i <= maxPage; i++) {
                if(i===pageNum){
                    pageList += '<a class="page-item page-current" href="#">'+i+'</a>';
                    continue;
                }
                pageList += '<a class="page-item" href="#">'+i+'</a>';
            };
        }

        return pageList;
    }
    //点击页码触发 XXXX！！！！
    Page.prototype.pageClick = function(pageNum){
        var pageList = this.pageGender(pageNum);
        // 非第一次
        if(!this.isFirst){
            this.initPage = pageNum;
            this.execFnNotByFirst.call(this, pageNum);
        }
        this.afterfn();

        return pageList;
    }
    //点击go
    Page.prototype.pageGo = function(){
        var self = this,
            pageValue;
        this.$paginationWrap.find('input').on('blur', function(event) {
            pageValue = $(this).val();
        });

        this.$paginationWrap.on('click', '.pages-go', function(event) {
            pageValue = self.utilNum(pageValue);
            if(pageValue<=self.maxPage){
                event.preventDefault();
                self.$paginationList.html( self.pageClick(pageValue) );
            }
        });
    }
    //上一页和下一页
    Page.prototype.preOrNext = function(){
        var self = this,
            pageValue;
        this.$paginationWrap.on('click', '.page-btn', function(event) {
            event.preventDefault();
            // 过滤空格
            // 或者使用示例对象的 当前页码属性 操作
            pageValue = Number(self.$paginationList.find('.page-current').html().replace(/(^\s+)|(\s+$)/g, '').replace(/\s/g, ''));
            if($(this).hasClass('btn-pre')){//上一页
                if(--pageValue<=0){
                    return;
                }
            }else if($(this).hasClass('btn-next')){//下一页
                if(++pageValue>=(self.maxPage+1)){
                    return;
                }
            }
            self.$paginationList.html( self.pageClick(pageValue) );
        });
    }
    //匹配input框页码格式，若正确则返回
    Page.prototype.utilNum = function(value){
        var regExp = /^[1-9]{1}[0-9]{0,}$/;
        if(regExp.test(value)){
            return Number(value);
        }
    }
    
    return Page;
});