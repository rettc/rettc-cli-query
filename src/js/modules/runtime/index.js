/**
 *  基于jade的runtime.js
 */
define(function () {
    var Url = require("../../interface/url");
    var cache = {};

    // 处理模板碎片中的jade变量（图片 和 html碎片）
    function checkData (fun, data){
        if (fun && data) {
            data.imgPath = Url.imgPath;
            data.htmlPath = Url.htmlPath;
        }
    }

    return function (options) {
        var beforeStart = options.beforeStart || function(){};
        var dtd = $.Deferred(beforeStart);
        var cache = {};
        
        // 加载模板
        function loadTpl(){
            var fun = cache[options.tplPath];
            var dtd = $.Deferred();
            if (!fun) {
                $.ajax({
                    url:Url.template+options.tplPath,//模板js的地址
                    dataType:'script'
                }).done(function(funStr, textStatus, jqXHR){
                    fun = eval("0, (" + funStr + ")");
                    cache[options.tplPath] = fun;
                    dtd.resolve(fun);
                }).fail(function(XHR, textStatus, errorThrown){
                    dtd.reject(XHR, textStatus, errorThrown);
                });
            }else{
                dtd.resolve(fun);
            }
            return dtd.promise();
        }
        // 加载数据
        function loadData(){
            var dtd = $.Deferred();
            $.ajax(options.param).done(function(msg, textStatus, jqXHR){
                dtd.resolve(msg);
            }).fail(function(XHR, textStatus, errorThrown){
                dtd.reject(XHR, textStatus, errorThrown);
            });
            return dtd.promise();
        }

        // 合成字符串
        $.when(loadTpl(), loadData()).done(function(fn, data){
            checkData(fn, data);
            var str = null;
            try{
               str = fn(data);
               dtd.resolve(str, data);//为了扩展把data传入，比如用于页码组件，用于优化判断维护状态
            }catch(err){
               dtd.reject(err);
            }
        }).fail(function(){
            dtd.reject();
        }).always(function(){});
        
        return dtd.promise();
    };
});
