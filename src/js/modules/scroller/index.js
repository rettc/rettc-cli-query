/**
 * 
 */
define(function () {
	var sHeight, wHeight, dHeight = 0;
	var timer = null;
	var array = [];
	$(window).bind("scroll", function () {
		if (timer) {
			window.clearTimeout(timer);
			timer = null;
		}
		sHeight = document.documentElement.scrollTop || document.body.scrollTop;	//滚动高度
		wHeight = document.documentElement.clientHeight;							//window
		dHeight = document.documentElement.offsetHeight;							//整个文档高度
		array.forEach(function(item){
			if (dHeight - (sHeight + wHeight) <= item.bHeight) {
				timer = window.setTimeout(function(){
					item.callBack();
					timer = null;
				},100);
			}
		});
	});

	return function () {
		var height = 0;
		var callBack = function(){};
		if (arguments.length === 1) {
			callBack = arguments[0];
		} else if (arguments.length === 2) {
			height = arguments[0];
			callBack = arguments[1];
		}
		array.push({
			bHeight : height,
			callBack : callBack
		});
	}
});