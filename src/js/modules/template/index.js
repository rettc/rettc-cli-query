/*
    自定义模板引擎
 */

define(function(){
    // 基于str扩展 简易模板引擎方法
    String.prototype.template = function(obj) {
        return this.replace(/\${\w+}\$/gi, function(matchs) {
            var returns = obj[matchs.replace(/\${/g, "").replace(/}\$/g, "")];
            return (returns + "") == "undefined"? "": returns;
        });
    }
});

/*
    使用示例
    
    第一步：引入此js文件
    第二步：前端定义模板碎片，有script, textarea, xmp, template(h5), json。。。。jade！！！！！！！ 或者 直接在页面标签上写，比如MVVM 类的框架
    第三步：传值解析

    数据之间的联动 和 通信 下次再说
 */