/**
 * 表单校验
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['./rule'], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory(require('./rule'));
    } else {
        root.formchecker = factory(root.rule);
    }
}(this, function (ruleWap) {
    var $inputs = null;
    var $tipCon = null;
    return function (wrapper) {
        var _wrapper = wrapper || "body";
        if ($tipCon) {
            $tipCon.html("").hide();
        }
        $tipCon = null;
        $inputs = $(_wrapper+" [formchecker]");

        var input = null;
        var rules = null;
        var rule = null;
        for (var i = 0, len = $inputs.length; i < len; i++) {
            input = $($inputs[i]);
            rules = input.attr('formchecker').split(",");
            for (var j = 0, jLen = rules.length; j < jLen; j++) {
                rule = ruleWap[rules[j].replace(/^\s|\s$/, '')];
                if (rule && !rule.check(input.val(),input)) {
                    $tipCon = $("#" + input.attr('errortipid'));
                    $tipCon && $tipCon.show().html(rule.errorInfo);
                    return false;
                }else{
                    $tipCon = $("#" + input.attr('errortipid'));
                    $tipCon && $tipCon.hide();
                }
            }
        }
        return true;
    };
}));