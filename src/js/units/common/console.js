/*
    console
 */

define(function(){
    var dateInstance = new Date(),
        year = dateInstance.getUTCFullYear() || dateInstance.getFullYear(),//年
        month = dateInstance.getUTCMonth()+1 || dateInstance.getMonth()+1,//月
        date = dateInstance.getUTCDate() || dateInstance.getDate(),//日
        day = (dateInstance.getUTCDay() || dateInstance.getDay()),//星期
        hh = dateInstance.getUTCHours() || dateInstance.getHours(),//时
        mm = dateInstance.getUTCMinutes() || dateInstance.getMinutes(),//分
        ss = dateInstance.getUTCSeconds() || dateInstance.getSeconds(),//秒
        ms = dateInstance.getUTCMilliseconds() || dateInstance.getMilliseconds();//毫秒
    if(!!console && !!console.log){
        console.log('UTC时间-%s是%d年%d月%d日%s星期%d时%d分%d秒%d毫秒', '今天', year, month, date, day, hh, mm, ss, ms);
        console.log('%crettc团队承建，欢迎合作', 'color: #283442; background: -webkit-linear-gradient(orange, green);background: -o-linear-gradient(orange, green);background: -moz-linear-gradient(orange, green);background: linear-gradient(orange, green); font-size: 24px;');
    }
});