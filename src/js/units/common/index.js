/**
 * 公用组件集合
 */
define(function () {
    return [
        require('./header'),
        require('./footer'),
        require('./console')
    ];
});
