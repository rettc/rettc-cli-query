/**
 * 页面主js
 */
define(function(){

    var self = null;

    var $header = $('.header'),
        $main = $('.main'),
        $footer = $('.footer'),
        $body = $('body'),
        $document = $(document),
        $window = $(window);

    return{
        _jobName:'main',
        check:function(){
            return true;
        },
        init:function(){
            self = this;
        },
        exec:function(){
            this.setPart();
            // 需要降频处理
            $window.on('resize', function(event){
                self.setPart();
            });

        },
        // 修正内容高度小于屏幕高度时，尾部不显示到底部的问题
        setPart:function(){
            if($body.height() < $document.height()){
                $main.height(function(){
                    return $document.height() - $header.height() - $footer.height();
                });
            }
        }
    }
});
