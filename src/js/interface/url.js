define(function(){

    var config = require('../config/index');

    var obj = {}

    if(config.lang=='en'){
        obj.imgPath='/en/images';
        obj.htmlPath='';
        obj.api = {}
    }else{
        obj.imgPath='/zh/images';
        obj.htmlPath='';
        obj.api = {}
    }
    obj.template = 'page/fragpage/'

    return obj;
});
