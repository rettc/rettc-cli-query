/**
 * 
 */
var webpack = require("webpack");
var glob = require("glob");
var path = require("path");

var webpackConfig = require("./webpack.config.js");
function getEntry() {
    var entry = {};
    glob.sync(__dirname + "/js/job/*.js").forEach(function (name) {
        var tempName = name.match(/([^/]+?)\.(coffee|js)$/)[1];
        entry[tempName] = "./js/job/" + tempName;
    });
    return entry;
}
var publicPathPerfix = process.env.STATIC || "";
module.exports = {
    context: __dirname,
    entry: getEntry(),
    output: {
        path: __dirname + "/build",
        filename: "[name].js",
        publicPath: "http://" + publicPathPerfix + "rettc.org/manage/job/"
    },
    module: {
        loaders: []
    },
    externals: {
        "jquery": "jQuery"
    },
    resolve: {
        modulesDirectories: [
            "node_modules",
            "bower_components"
        ],
        extensions: ['', '.js', '.json', '.less', '.css']
    },
    plugins: [
        new webpack.optimize.DedupePlugin()
    ]
};
